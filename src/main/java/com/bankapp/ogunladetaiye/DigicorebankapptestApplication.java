package com.bankapp.ogunladetaiye;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicorebankapptestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigicorebankapptestApplication.class, args);
    }

}
